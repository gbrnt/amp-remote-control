"""
Modify spur gear from https://www.thingiverse.com/thing:2358211/files
Adapt it to connect to a potentiometer in order to adjust volume
"""

from solid import *
from solid.utils import *

SEGMENTS=48

# Gear from the link in the comment at the top of the file
gear = down(2.5)(rotate([0, -90, 0])(import_stl("spurgear.stl")))

# Outer cylinder and holes in the top and bottom for servo's splines and potentiometer
main_cyl = cylinder(d=12, h=13)
splines_hole = down(0.5)(cylinder(d=7, h=5.5))
pot_hole = cylinder(d=6.1, h=13.5)

# Combine to make an adapter
adapter = gear + down(5)(main_cyl-splines_hole-pot_hole)

# Cut hole for grub screw
grub_screw_hole = up(4)(rotate([90, 0, 45])(cylinder(d=2.6, h=10)))
adapter -= grub_screw_hole

# Cube to add flats to manually turn the adapter
flats_size = 11
flats_cube = translate([-flats_size/2, -flats_size/2, -5])(cube([flats_size, flats_size, 13]))
adapter *= flats_cube;

scad_render_to_file(adapter, "sg90-pot-adapter.scad", file_header=f"$fn={SEGMENTS};\n")
