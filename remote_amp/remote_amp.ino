/*
 *  Remote control of audio amp using TV remote's volume control
 *  Listens for TV remote's IR signals and adjusts digital potentiometer attached to amp
 */

#define DECODE_SAMSUNG
#include <IRremote.hpp>
#include <Servo.h>

#define IR_RECEIVE_PIN 2
#define SERVO_PIN 10

uint32_t data;
uint8_t volume;

// Set of volume levels with increased sensitivity at low end
uint8_t volume_levels[] = {
  0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180

};
uint8_t default_volume_index = 3;
uint8_t volume_count = sizeof(volume_levels)/sizeof(uint8_t);
uint8_t volume_index;
bool is_muted;

Servo servo;
int servo_pos = 0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  IrReceiver.begin(IR_RECEIVE_PIN, ENABLE_LED_FEEDBACK);
  Serial.begin(115200);

  // Initialise volume to low default value
  volume = volume_levels[default_volume_index];
  volume_index = default_volume_index;

  // Attach servo object to PWM pin, and reset volume to 0
  servo.attach(SERVO_PIN);
  servo.write(volume);
}


void loop() {
  int8_t change;

  if (IrReceiver.decode()) {
    data = IrReceiver.decodedIRData.decodedRawData;
    //IrReceiver.printIRResultShort(&Serial);
    //Serial.println(data);

    switch(data) {
    case 0xF8070707:
      //Serial.println("Vol Up");
      volume = changeVolume(1);
      if (!is_muted) servo.write(volume);
      printVolume();
      break;
    case 0xF40B0707:
      //Serial.println("Vol Down");
      volume = changeVolume(-1);
      if (!is_muted) servo.write(volume);
      printVolume();
      break;
    case 0xF00F0707:
      is_muted = !is_muted;
      if (is_muted) servo.write(0);
      else servo.write(volume);
      printVolume();
      break;
    default:
      //IrReceiver.printIRResultShort(&Serial);
      break;
    }

    IrReceiver.resume();
  }
}


uint8_t changeVolume(int8_t change) {
  /*
   *  Return volume at next index, stopping at min/max
   */

  // Start with current volume index
  int8_t idx = volume_index + change;

  idx = max(min(idx, volume_count-1), 0);
  volume_index = idx;

  return volume_levels[volume_index];
}


void printVolume(void) {
  /*
   *  Print the current volume setting
   *  If muted, print M(volume)
   */

  if (is_muted) {
    Serial.print("M(");
    Serial.print(volume);
    Serial.println(")");
  }
  else {
    Serial.println(volume);
  }
}
